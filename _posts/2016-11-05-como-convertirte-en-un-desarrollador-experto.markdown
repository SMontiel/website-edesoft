---
title: Cómo convertirte en un desarrollador experto
layout: post
subtitle: Aunque cuando estás estudiando sólo tienes la mente puesta en aprobar asignaturas...
date: '2016-11-05 06:00:00 -0600'
author: Salvador Montiel
header-img: http://edesoft.tk/img/portfolio-2.jpg
---

### ¿Cómo me convierto en un desarrollador experto?

Aunque cuando estás estudiando sólo tienes la mente puesta en aprobar asignaturas (y dedicar mucho al tiempo al ocio…), es importante que pienses que cuando termines tus estudios tienes que empezar a trabajar, y es muy importante que lo pienses.
El mercado laboral es completamente diferente al mundo de la universidad y si queremos empezar con buen pie en el mismo tenemos que trabajar duro desde cero.

### El primer trabajo es crucial

No es lo mismo entrar en una empresa que sólo busque chuparte la sangre a base de horas y horas extras, donde tu trabajo será muy repetitivo, que entrar en otra donde se mime y se cuide la calidad del software, donde se ponga empeño por hacer las cosas bien.

Normalmente al segundo tipo de empresa es más complicado acceder, y sólo acceden los que destacan, aunque sea un poco. Nosotros queremos encontrar trabajo en esa empresa, en la de tipo 2.

### Investiga por tu cuenta

Normalmente cuando terminas tus estudios diarios sueles soltarlo todo y dedicarte a otra cosa, y eso está muy bien. Pero si quieres empezar a coger experiencia te aconsejo que busques en qué plataforma de desarrollo te encuentras más cómodo y empieces a investigar cuáles son las empresas que trabajan con ella. Mira cómo lo hacen e intenta imitar sus metodologías de desarrollo.

### Fomenta tu creatividad
![Improve your brain](http://devexperto.com/wp-content/uploads/2016/05/brain-idea-e1464853775416.jpg){:class="img-responsive"}

La mejor forma de poner en práctica tus conocimientos y emplearlos es fomentando tu creatividad. Inicialmente si no se te ocurren muchas cosas, puedes empezar buscando una aplicación que te guste e intentar copiar su funcionamiento.

Esto te va a ayudar muchísimo porque te empezarás a encontrar con los problemas típicos de cualquier plataforma, tendrás que buscar las soluciones y eso será experiencia que irás ganando poco a poco que, créeme, vale muchísimo.

En mi caso particular intentaba aprender algo de cada plataforma: que estaba haciendo una aplicación para el móvil y necesitaba un backend, pues a buscarse formas de hacer la parte de backend. Buscaba frameworks, APIs…

Al principio abruma un poco la cantidad de conceptos que empiezas a aprender, pero rápidamente te haces con lo básico y empiezas a hacer cosas interesantes.

### Intenta hacer las cosas bien

Pero a ver Sergio, ¿esto es trivial no? Pues no, cuando digo hacer las cosas bien no es que programes un botón y directamente funcione. Plantéate esto: Funciona, pero… ¿lo estoy haciendo de la forma correcta?.

El siguiente paso es buscar a las empresas que encontraste en el punto 1 y ver si realmente lo hacen como tú. Contrólate un poco en este sentido, antes de correr hay que andar. Este punto te ayudará a convertirte en un desarrollador, no en un programador.

### Súbelo TODO a Github
![Github Octocat](https://devexperto.com/wp-content/uploads/2016/05/github-logo.png){:class="img-responsive"}

A no ser que tengas una idea super revolucionaria que creas que pueden vender por miles de millones de euros (todas, vamos 😉 ), súbelo todo a Github. Es el lugar por excelencia donde toda la gente sube su código open source. Puedes aprender muchísimo del código que hay allí (de hecho todo lo que aprenderás vendrá de ahí, o al menos el 90%).

Esto tiene una ventaja oculta pero importantísima: te empiezas a familiarizar con Git, el sistema de control de versiones por excelencia en la actualidad (o al menos uno de los más populares).

Tú puedes ser muy buen desarrollador pero si no te sabes manejar con un sistema de control de versiones tienes un grave problema. Como anécdota en este punto, a mí me han llegado a decir “¿Si ya tengo Google Drive o Dropbox, para qué quiero un sistema de control de versiones?”. En cuanto empieces a investigar un poco lo que es, te darás cuenta de lo insultante que es esa pregunta.

Siempre que alguna empresa me ha contactado me han dicho: “Sí, lo que dices está muy bien pero… ¿nos pasas tu Github?”

De hecho, podéis ver mi Github, no es nada del otro mundo, pero al menos hay repositorios de varias plataformas.

### Apúntate a todos los concursos que puedas

Este punto es crucial, aprenderás muchísimo porque el concurso te lo exigirá, y si tienes la suerte de ganarlo lo podrás incluir en tu currículum y será un factor diferencial con respecto al resto. Aunque claro, ganar concursos no es fácil ni muchísimo menos.

### Conclusiones

Ponte en el lugar de una empresa, a un lado tiene decenas de currículums de gente donde pone:

**Formación académica**

* Técnico en Electrónica de consumo
* Técnico en Admin. Sistemas informáticos
* Graduado en Ingeniería de Software

Y en el otro lado tiene el tuyo con (por ejemplo, si te has decidido por Android):

**Formación académica**

* Técnico en Electrónica de consumo
* Técnico en Admin. Sistemas informáticos
* Graduado en Ingeniería de Software

**Experiencia**

* Github: [github.com/SMontiel](http://github.com/SMontiel)
* Aplicación 1, Aplicación 2, Aplicación 3
* Experiencia en Retrofit, Clean Architecture, custom views, Android Framework…
* Premio en el concurso…
* Charla impartida en…

No hace falta que te diga a quien van a llamar, ¿no?

Espero que estos pequeños consejos te sirvan para empezar con buen pie tu carrera profesional y que así consigas llegar a ser un experto.
